import { Routes, RouterModule } from '@angular/router';

import { NgModule } from '@angular/core';
import { LayoutTodoListComponent } from './todo-list/layout-todo-list/layout-todo-list.component';
import { LayoutSkillsComponent } from './skill/layout-skills/layout-skills.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutSkillsComponent
  },
  {
    path: 'my-skills',
    component: LayoutSkillsComponent
  },
  {
    path: 'todo-list',
    component: LayoutTodoListComponent
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ],
  declarations: []
})
export class AppRoutingModule {

  constructor() { }
}
