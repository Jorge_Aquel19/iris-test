import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule } from '@angular/material/button';
import { MatListModule } from '@angular/material/list';
import { MatCheckboxModule } from '@angular/material/checkbox';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { LayoutTodoListComponent } from './todo-list/layout-todo-list/layout-todo-list.component';
import { FormCreateOptionComponent } from './todo-list/form-create-option/form-create-option.component';
import { ItemListComponent } from './todo-list/item-list/item-list.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { LayoutSkillsComponent } from './skill/layout-skills/layout-skills.component';
import { SkillsListComponent } from './skill/skills-list/skills-list.component';

@NgModule({
  declarations: [
    AppComponent,
    LayoutTodoListComponent,
    FormCreateOptionComponent,
    ItemListComponent,
    LayoutSkillsComponent,
    SkillsListComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatButtonModule,
    MatListModule,
    MatCheckboxModule
  ],
  providers: [],
  bootstrap: [ AppComponent ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppModule { }
