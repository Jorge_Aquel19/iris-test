import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LayoutSkillsComponent } from './layout-skills.component';

describe('LayoutSkillsComponent', () => {
  let component: LayoutSkillsComponent;
  let fixture: ComponentFixture<LayoutSkillsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LayoutSkillsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LayoutSkillsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
