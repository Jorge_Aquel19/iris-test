import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-layout-skills',
  templateUrl: './layout-skills.component.html',
  styleUrls: ['./layout-skills.component.scss']
})
export class LayoutSkillsComponent implements OnInit {

  public skillsFrontend = [ 'HTML', 'CSS', 'JavaScript', 'Typescript', 'Angular 2+', 'Angular Material', 'Bootstrap' ];
  public skillsBackend = [ 'Nodejs', 'Nestjs', 'Express', 'TypeOrm', 'PHP', 'Slim 3 Framework' ];
  public otherSkilss = [ 'Mysql', 'GIT' ];

  constructor() { }

  ngOnInit(): void {
  }

}
