import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormCreateOptionComponent } from './form-create-option.component';

describe('FormCreateOptionComponent', () => {
  let component: FormCreateOptionComponent;
  let fixture: ComponentFixture<FormCreateOptionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormCreateOptionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormCreateOptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
