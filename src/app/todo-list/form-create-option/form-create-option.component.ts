import { ChangeDetectionStrategy, Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-form-create-option',
  templateUrl: './form-create-option.component.html',
  styleUrls: [ './form-create-option.component.scss' ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormCreateOptionComponent implements OnInit {

  @Output() sendNewItem = new EventEmitter<any>();
  @Output() sendFilter = new EventEmitter<string>();

  filter = new FormControl('All');
  newItem = new FormControl();

  filterOptions = [ 'All', 'In Progress', 'Done' ];

  constructor() { }

  /**
   * @description
   * Función que emite la información del formulario
   */
  onSendNewItem(): void {
    this.sendNewItem.emit({ name: this.newItem.value, status: false });
    this.newItem.setValue('');
  }

  /**
   * @description
   * Función que emite el valor del filtro
   */
  onSendFilter(): void {
    this.sendFilter.emit(this.filter.value);
  }

  ngOnInit(): void {
  }
}
