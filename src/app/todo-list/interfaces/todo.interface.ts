export interface TodoInterface {
  name: string;
  status: boolean;
}
