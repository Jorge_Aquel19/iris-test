import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { TodoInterface } from '../interfaces/todo.interface';

@Component({
  selector: 'app-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: [ './item-list.component.scss' ]
})
export class ItemListComponent implements OnInit, OnChanges {

  @Input() itemList: TodoInterface[] = [];
  @Output() sendItem = new EventEmitter<TodoInterface>();
  @Output() sendDeleteItem = new EventEmitter<TodoInterface>();

  constructor() { }

  /**
   * @description
   * Función que emite un item de la lista
   * @param item Contiene un objeto de tipo TodoInterface
   */
  onSendItem(item: TodoInterface): void {
    this.sendItem.emit(item);
  }

  /**
   * @description
   * Función que emite un item a eliminar
   * @param item Contiene un objeto de tipo TodoInterface
   */
  onSendDeleteItem(item: TodoInterface): void {
    this.sendDeleteItem.emit(item);
  }

  ngOnInit(): void { }

  ngOnChanges(changes: SimpleChanges): void {
    this.changesItemList(changes);
  }

  /**
   * @description
   * Función que observa los cambos de la propiedad itemList
   * @param changes Contiene un objeto de tipo SimpleChanges
   */
  private changesItemList(changes: SimpleChanges): void {
    console.log(changes);
  }
}
