import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LayoutTodoListComponent } from './layout-todo-list.component';

describe('LayoutTodoListComponent', () => {
  let component: LayoutTodoListComponent;
  let fixture: ComponentFixture<LayoutTodoListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LayoutTodoListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LayoutTodoListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
