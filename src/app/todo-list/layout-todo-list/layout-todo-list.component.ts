import { Component, OnInit } from '@angular/core';

import Swal from 'sweetalert2';

import { TodoInterface } from '../interfaces/todo.interface';

@Component({
  selector: 'app-layout-todo-list',
  templateUrl: './layout-todo-list.component.html',
  styleUrls: [ './layout-todo-list.component.scss' ]
})
export class LayoutTodoListComponent implements OnInit {


  itemList: TodoInterface[] = [];
  auxItemList: TodoInterface[] = [];
  filter: string = 'ALL';

  constructor() { }

  /**
   * @description
   * Función que obtiene el nuevo item
   * @param newItem Contiene un objeto de tipo any
   */
  onGetNewItem(newItem: TodoInterface): void {

    if (newItem.name && !this.validNameItem(newItem)) {
      this.itemList.push(newItem);
      this.auxItemList.push(newItem);
    } else if (newItem.name) {
      Swal.fire({
        title: 'Advertencia!',
        text: `Ya existe una tarea con el nombre ${newItem.name}.`,
      });
    }
  }

  /**
   * @description
   * Función que obtiene el tipo de filtro
   * @param filter Contiene un string
   */
  onGetFilter(filter: string): void {
    this.filter = filter;
    this.itemList = this.filterItem();
  }

  /**
   * @description
   * Función que obtiene el item al cual se le va a cambiar el status
   * @param item Contien un objeto de tipo TodoInterface
   */
  onGetItem(item: TodoInterface): void {
    this.changeStatusItem(item);
    this.itemList = this.filterItem();
  }

  /**
   * @description
   * Función que obtiene un item a eliminar
   * @param item Contien un objeto de tipo TodoInterface
   */
  onGetDeleteItem(item: TodoInterface): void {
    this.deleteItem(item);
    this.itemList = this.filterItem();
  }

  ngOnInit(): void { }

  /**
   * @description
   * Función que valida el nombre del item
   * @param newItem Contiene un objeto de tipo itemToChange
   * @returns un boolean
   */
  private validNameItem(newItem: TodoInterface): boolean {
    return this.auxItemList.some((item: TodoInterface) => newItem.name === item.name);
  }

  /**
   * @description
   * Función que cambia es estatus de un item
   * @param itemToChange Contiene un objeto de tipo itemToChange
   */
  private changeStatusItem(itemToChange: TodoInterface): void {
    this.auxItemList.forEach((curItem: TodoInterface) => {
      if (itemToChange.name === curItem.name) {
        curItem.status = !curItem.status;
      }
    });
  }

  /**
   * @description
   * Función que elimina un item de la lista
   * @param itemToDelete Contiene un objeto de tipo itemToChange
   */
  private deleteItem(itemToDelete: TodoInterface): void {
    const index = this.auxItemList.findIndex((item: TodoInterface) => itemToDelete.name === item.name);
    this.auxItemList.splice(index, 1);
  }

  /**
   * @description
   * Función que filtra las tareas por su estado
   * @param filterType
   * @returns un array de tipo TodoInterface
   */
  private filterItem(): TodoInterface[] {

    if (this.filter === 'All') {
      return this.auxItemList;
    } else if (this.filter === 'In Progress') {
      return this.auxItemList.filter((item: TodoInterface) => !item.status);
    } else if (this.filter === 'Done') {
      return this.auxItemList.filter((item: TodoInterface) => item.status);
    }

    return this.auxItemList;
  }
}
